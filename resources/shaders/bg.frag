#version 400

in vec2 TexCoords;

uniform sampler2D texture_diffuse1;
uniform float time;

void main()
{    
	vec2 p = (vec2(4., 1.)*TexCoords - vec2(0., 0.33))*0.15;
	vec3 color = texture(texture_diffuse1, TexCoords).rgb;
	vec3 pos = vec3(p.x, 0.25, p.y*0.5);
	pos.z += p.x / p.y * p.x / pos.y;
	pos.z += .70*p.x / p.y * 0.71*p.x / pos.y*-2.0;

	vec2 sur = vec2(pos.x / pos.z, pos.y / pos.z)*0.5;
	sur.y -= 3.;
	float w = -0.5*sign((mod(sur.x, 2.0) - .5) * (mod(sur.y, 2.0) - 01.8));
	w *= pos.z*pos.z*7.0;

	sur.y -= -time*2.;

	float e = sign((mod(sur.x, .50) - .10) * (mod(sur.y, .5) - 0.1));
	e *= pos.z*pos.z*80.0;

	color.r = w + e + 3.0;
	color.g = w + e + 2.0;
	color.b = w + e;

	vec3 fcolor = color * vec3(e + w / 20.0, e + w, 0);

	gl_FragColor = vec4(fcolor, 1.0);
}