#ifndef PLAYER_H
#define PLAYER_H

#include "GL\glew.h"
#include "GL\wglew.h"

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include "SFML\Graphics.hpp"
#include "SFML\OpenGL.hpp"

#include "util\KeyHandler.h"

#include "Shader.h"
#include "Mesh.h"
#include "Model.h"

class Player
{
public:

	/// <summary>
	/// Default player constructor
	/// </summary>
	/// <param name="elapsed"> time between frames </param>
	/// <param name="keyHandler"> keyhandler for key press checking </param>
	/// <param name="projection"> projection matrix </param>
	/// <param name="view"> camera view matrix </param>
	/// <param name="path"> c-style string to model directory </param>
	/// <param name="vertPath"> c-style string to vertex shader directory </param>
	/// <param name="fragPath"> c-style string to fragment shader directory </param>
	/// <param name="position"> glm 3d coordinate vector that defines player position </param>
	Player(
		const sf::Time& elapsed
		, const KeyHandler& keyHandler
		, const glm::mat4& projection
		, const glm::mat4& view
		, const GLchar* path
		, const GLchar* vertPath
		, const GLchar* fragPath
		, const glm::vec3& position
	);

	/// <summary>
	/// Default destructor,
	/// removes member variables off the stack
	/// </summary>
	~Player();

	/// <summary>
	/// main player update logic
	/// </summary>
	void update();

	/// <summary>
	/// main player render logic
	/// </summary>
	void render();

	/// <summary>
	/// gets the players current position
	/// </summary>
	/// <returns> returns constant reference to glm 3d coord vector </returns>
	const glm::vec3& getPosition() const;

	/// <summary>
	/// gets the players current size
	/// </summary>
	/// <returns> returns constant reference to glm 3d coord vector </returns>
	const glm::vec3& getSize() const;

	/// <summary>
	/// sets player posiion using sfml 3d coord vector
	/// </summary>
	/// <param name="pos"> constant reference to sfml 3d coord vector </param>
	void setPosition(const sf::Vector3f& pos);
	/// <summary>
	/// sets player posiion using glm 3d coord vector
	/// </summary>
	/// <param name="pos"> constant reference to glm 3d coord vector </param>
	void setPosition(const glm::vec3& pos);
	/// <summary>
	/// sets player position using each float as (x,y,z) respectively
	/// </summary>
	/// <param name="x"> constant reference to the float, x coordinate </param>
	/// <param name="y"> constant reference to the float, y coordinate </param>
	/// <param name="z"> constant reference to the float, z coordinate </param>
	void setPosition(const float& x, const float& y, const float& z);

	/// <summary>
	/// moves player position by a offset
	/// </summary>
	/// <param name="offset"> constant reference to sfml 3d coord vector </param>
	void move(const sf::Vector3f& offset);
	/// <summary>
	/// moves player position by a offset
	/// </summary>
	/// <param name="offset"> constant reference to glm 3d coord vector </param>
	void move(const glm::vec3& offset);
	/// <summary>
	/// moves player position by a offset
	/// </summary>
	/// <param name="offsetX"> constant reference to the float, x coordinate offset </param>
	/// <param name="offsetY"> constant reference to the float, y coordinate offset </param>
	/// <param name="offsetZ"> constant reference to the float, z coordinate offset </param>
	void move(const float& offsetX, const float& offsetY, const float& offsetZ);


private:
	/// <summary>
	/// processes input using the reference to the keyhandler
	/// for key press checks
	/// </summary>
	void processInput();

	// reference to game loop time step
	const sf::Time& m_elapsed;

	// reference to key handler for input
	const KeyHandler& m_keyHandler;

	/* 3D MODEL MATRIXS */
	// Projection matrix
	const glm::mat4& m_projectionMat;
	// Camera matrix
	const glm::mat4& m_viewMat;
	// normal matrix
	glm::mat4 m_normalMat;
	// Model matrix
	glm::mat4 m_modelMat;

	// 3D models shader
	Shader m_shader;
	// 3D model
	Model m_model;

	// player position
	glm::vec3 m_position;
	// player size
	glm::vec3 m_size;
};

#endif // !PLAYER_H