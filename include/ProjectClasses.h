#ifndef _PROJECT_CLASSES_H
#define _PROJECT_CLASSES_H

/////////////////////////////////////////////////
// This header is used to include
//  all project classes for the game
/////////////////////////////////////////////////

// utility classes
#include "util\KeyHandler.h"

// Camera controller class
#include "Camera.h"

// 2D plane with our custom frag shader
#include "Background.h"

// 3D model rendering
#include "Model.h"
#include "Shader.h"

// Enemies manager class
#include "Enemies.h"

// Player controlled class
#include "Player.h"

#endif // !_PROJECT_CLASSES_H