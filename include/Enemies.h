#ifndef ENEMIES_H
#define ENEMIES_H

#include <memory>

#include "GL\glew.h"
#include "GL\wglew.h"

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include "SFML\Graphics.hpp"
#include "SFML\OpenGL.hpp"

#include "Shader.h"
#include "Mesh.h"
#include "Model.h"

class Enemies
{
private:
	struct Enemy
	{
		// default constructor
		Enemy()
			: m_position(0.0f)
			, m_size(1.0f)
		{};
		Enemy(
			const glm::vec3& pos
			, const glm::vec3& size
		)
			: m_position(pos)
			, m_size(size)
		{}

		glm::vec3 m_position;
		glm::vec3 m_size;
	};
public:
	Enemies(
		const sf::Time&
		, const glm::mat4&
		, const glm::mat4&
		, const GLchar*
		, const GLchar*
		, const GLchar*
	);
	~Enemies();

	void update();
	void render();
	bool checkCollisions(const glm::vec3&);

	void erase(const int&);

	void restart();

	static void setGameOver();

private:
	void spawn();

	bool checkCollision(const glm::vec3&, const Enemy&);

	static sf::Time s_timeTracker;
	static bool s_gameOver;
	static const float s_START_SPEED;
	static float s_speed;


	// spawning dimensions
	const glm::vec3 SPAWN_SIZE = glm::vec3(1.0f, 1.0f, 1.0f);
	// spawning position
	const glm::vec3 SPAWN_POS = glm::vec3(0.0f, -2.0f, 3.0f);
	// distance from player which cube spawns
	const float SPAWN_DIST = 100.0f;
	// Distance that enemies de-spawn
	const float m_DESPAWN_DIST = 10.0f;
	// hitbox size
	const glm::vec3 m_HIT_BOX_SIZE = glm::vec3(0.75f, 0.0f, 1.7f);

	// time between updates
	const sf::Time& m_ELAPSED;
	// projection matrix
	const glm::mat4& m_PROJECTION;
	// view matrix
	const glm::mat4& m_VIEW;
	// identity matrix
	const glm::mat4 m_IDENTITY;
	// transform matrix
	glm::mat4 m_transform;

	// 3D model
	Model m_model;
	// 3D models shader
	Shader m_shader;

	// timer for enemies
	sf::Time m_spawnTrack;
	// amount of time between enemies' spawns
	sf::Time m_spawnTime;

	// vector of unique pointer to enemy struct (position, size)
	std::vector<std::unique_ptr<Enemy>> m_enemies;

};

#endif // !ENEMIES_H