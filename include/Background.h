#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <memory>

#include "GL\glew.h"
#include "GL\wglew.h"

#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

#include "SFML\System\Time.hpp"

#include "util\Debug.h"
#include "GL\glew.h"

#include "Model.h"

class Background
{
public:
	Background(
		const sf::Time& time
		, const glm::mat4& projection
		, const GLchar* modelPath
		, const GLchar* vertPath
		, const GLchar* fragPath
		, const glm::vec3& position = glm::vec3(0.0f)
		, const float& width = 1.0f
		, const float& height = 1.0f
	);
	~Background();

	void update();
	void render();

private:
	// background shader
	Shader m_shader;
	// Contains our background model
	Model m_model;

	glm::vec3 m_position;
	float m_width;
	float m_height;

	/* 3D MODEL MATRIXS */
	// Projection matrix
	const glm::mat4 & m_projectionMat;
	// Camera matrix
	const glm::mat4 m_viewMat;
	// Model matrix
	glm::mat4 m_modelMat;

	const sf::Time& m_ELAPSED;
};

#endif // !BACKGROUND_H