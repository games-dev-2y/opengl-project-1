#ifndef GUI_H
#define GUI_H

#include "SFML\Graphics\RenderTarget.hpp"
#include "SFML\Graphics\Text.hpp"

class GUI
{
public:
	GUI();
	~GUI();
	void render(sf::RenderTarget &);
private:
	sf::Font m_font;
	sf::Text m_text;
};

#endif // !GUI_H