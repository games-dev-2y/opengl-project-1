//Undefine DEBUG
#ifdef DEBUG
#undef DEBUG
#endif
// dynamically change from debug to release
#ifdef _DEBUG
//Define DEBUG 1 or 2 for greater level of debug messages
#define DEBUG 1
#else
#define DEBUG 0
#endif
// include standard output stream
#include <iostream>
#include <string>
//MACRO for streaming DEBUG
#if defined DEBUG
#if (DEBUG >= 1)
#define DEBUG_MSG(x) (std::cout << (x) << std::endl)
#else
#define DEBUG_MSG(x)
#endif
#else
#define DEBUG_MSG(x)
#endif