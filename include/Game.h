#ifndef GAME
#define GAME

// utility Debug class
#include "util\Debug.h"
// opengl wrangler library
#include "GL\glew.h"
#include "GL\wglew.h"

// opengl math library
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

// SFML Window and OpenGL libraries
#include "SFML\Window.hpp"
#include "SFML\OpenGL.hpp"

// standard memory library
// needed for using smart pointers
#include <memory>

// Project classes
#include "ProjectClasses.h"

/// <summary>
/// Main Entry Point for Game Application
/// </summary>
class Game
{
public:
	Game(sf::ContextSettings settings = sf::ContextSettings());
	~Game();
	void run();
private:
	void processEvents();
	void update();
	void render();

	void initialize();

	void checkCollision();
	void checkCollision(Player& p);

	void loadAssets();

	enum class States
	{
		Loading, Splash, Game, Over
	}m_state;

	void setGameState(const States&);

	// Main Game window
	sf::RenderWindow m_window;

	// main game internal clock
	sf::Clock m_clock;
	// time elapsed between last update call
	sf::Time m_elapsed;

	/* Game constants */
	// time between each frame
	const sf::Time TIME_PER_FRAME = sf::seconds(1.0f / 60.0f);
	// wait time in splash screen
	const sf::Time WAIT_TIME = sf::seconds(3.0f / 60.0f);
	// Game boundaries
	const glm::vec2 m_GAME_BOUNDS = glm::vec2(8.0f, 8.0f);

	/* File path to our shaders */
	// path for all resources
	const std::string m_FILE_PATH = ".\\resources\\";
	// path for all shaders
	const std::string m_SHADER_FOLDER = m_FILE_PATH + "shaders\\";
	// path for all textures
	const std::string m_TEXTURE_FOLDER = m_FILE_PATH + "textures\\";
	// path for all fonts
	const std::string m_FONT_FOLDER = m_FILE_PATH + "fonts\\";
	// path for all models
	const std::string m_MODEL_FOLDER = m_FILE_PATH + "models\\";
	// background vertex shader
	const std::string m_BG_VERTEX_SHADER_FILE = m_SHADER_FOLDER + "bg.vert";
	// background fragment shader
	const std::string m_BG_FRAG_SHADER_FILE = m_SHADER_FOLDER + "bg.frag";
	// model Vertex Shader
	const std::string m_MODEL_VERTEX_SHADER_FILE = m_SHADER_FOLDER + "model.vert";
	// model Fragment Shader
	const std::string m_MODEL_FRAG_SHADER_FILE = m_SHADER_FOLDER + "model.frag";
	// font
	const std::string m_FONT_FILE = m_FONT_FOLDER + "ARBERKLEY.ttf";
	// background image
	const std::string m_SPRITE_FILE = m_TEXTURE_FOLDER + "splash_screen_bg.jpg";
	// background model
	const std::string m_BG_OBJ = m_MODEL_FOLDER + "background/background.obj";
	// player model
	const std::string m_PLAYER_OBJ = m_MODEL_FOLDER + "spaceship/spaceship.obj";
	// enemy model
	const std::string m_ENEMY_OBJ = m_MODEL_FOLDER + "missile/missile.obj";

	/* default cube parameters */
	// starting position
	const glm::vec3 START_POS = glm::vec3(0.0f, -2.0f, 3.0f);
	// default width
	const float START_WIDTH = 1.0f;
	// default height
	const float START_HEIGHT = 1.0f;
	// default depth
	const float START_DEPTH = 1.0f;
	/* Game constants */

	/* default background parameter */
	// position
	const glm::vec3 m_BG_POS = glm::vec3(0.0f, 0.0f, -4.0f);
	// default width
	const float m_BG_WIDTH = 12.0f;
	// default height
	const float m_BG_HEIGHT = 12.0f;

	/* default camera parameters */
	// projection
	const glm::mat4 m_PROJECTION = glm::perspective(
		45.0f,					// Field of View 45 degrees
		4.0f / 3.0f,			// Aspect ratio
		1.0f,					// Display Range Min : 0.1f unit
		100.0f					// Display Range Max : 100.0f unit
	);
	// position
	const glm::vec3 m_CAM_POS = glm::vec3(0.0f, 6.0f, 10.0f);
	const glm::vec3 m_CAM_TARGET = glm::vec3(0.0f, 0.0f, 0.0f);

	// Keyhandler mapping of all inputs
	KeyHandler m_keyHandler;

	// Camera for our game
	// referenced in multiple game objects
	// for projection calculations
	Camera m_camera;

	// unique pointer to our background
	std::unique_ptr<Background> m_background;

	// unique pointer to our player
	std::unique_ptr<Player> m_player;

	// vector of shared pointers to our enemies
	std::unique_ptr<Enemies> m_enemies;

	// timer since game started
	sf::Time m_gameTime;
	// splash screen timer
	sf::Time m_splashTimer;

	// Game over bool
	bool m_gameOver;
	// slow down tunnel effect timer
	sf::Time m_gameOverTime;
	// cube fading out effect
	float m_fadeOut;

	// obj loading error message
	std::string m_objError;
	
	// Font file
	sf::Font m_font;
	// Textbox
	sf::Text m_text;
	// background image
	sf::Texture m_texture;
	// background sprite
	sf::Sprite m_sprite;
};

#endif // !GAME