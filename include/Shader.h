#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include "GL\glew.h"

#include "util\Debug.h"

class Shader
{
public:
	GLuint m_program;

	Shader(const GLchar*, const GLchar*);
	~Shader();

	void Use();
};
#endif // !SHADER_H