#ifndef _CAMERA
#define _CAMERA

// including external dependencies
#include "GL\glew.h"
#include "GL\wglew.h"
#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"

class Camera
{
public:
	Camera(
		const glm::vec3 & position = glm::vec3(0.0f)
		, const glm::vec3 & target = glm::vec3(0.0f)
	);
	~Camera();

	//void update();

	void move(const glm::vec3 &);
	void lookAt(const glm::vec3 &);

private:
	// camera's viewing angle
	const glm::vec3 m_LOOK_UP = glm::vec3(0.0f, 1.0f, 0.0f);

	// camera's position in World Space
	glm::vec3 m_position;

	// camera's target in World Space
	glm::vec3 m_lookingAt;

	// camera's 4x4 view matrix
	glm::mat4 m_camera;

public:
	// 4x4 matrix of the camera's view fustrum
	// where the camera is viewing
	// from a position towards a target
	const glm::mat4 & m_CAMERA;
};

#endif // !_CAMERA