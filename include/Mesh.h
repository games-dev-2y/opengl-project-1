#ifndef MESH_H
#define MESH_H

#include "GL\glew.h"
#include "GL\wglew.h"

#include <vector>
#include <sstream>
#include "glm\glm.hpp"

#include "util\assimp\Importer.hpp"
#include "Shader.h"

struct Vertex
{
	glm::vec3 m_position;
	glm::vec3 m_normal;
	glm::vec2 m_texCoords;
};

struct Texture
{
	GLuint m_id;
	std::string m_type;
	aiString m_path;
};

class Mesh
{
public:

	/* Mesh Data */
	std::vector<Vertex> m_vertices;
	std::vector<GLuint> m_indices;
	std::vector<Texture> m_textures;

	/* Functions */
	Mesh(
		const std::vector<Vertex>&
		, const std::vector<GLuint>&
		, const std::vector<Texture>&
	);
	~Mesh();

	void draw(const Shader&);

private:
	/* Render data */
	GLuint m_vao, m_vbo, m_ebo;

	/* Functions */
	void setupMesh();
};

#endif // !MESH_H