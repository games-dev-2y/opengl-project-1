#ifndef MODEL_H
#define MODEL_H

#include "GL\glew.h"
#include "GL\wglew.h"

#include <vector>
#include <sstream>

#include "SOIL.h"
#include "util\assimp\Importer.hpp"
#include "util\assimp\scene.h"
#include "util\assimp\postprocess.h"

#include "util\Debug.h"
#include "Shader.h"
#include "Mesh.h"

GLint TextureFromFile(const char* path, std::string directory);

class Model
{
public:
	/* Functions */
	Model(const GLchar * path);
	~Model();

	void draw(const Shader&);

private:
	/* Model data */
	std::vector<Mesh> m_meshes;
	std::string m_directory;
	std::vector<Texture> m_texturesLoaded;

	/* Functions */
	void loadModel(const std::string& path);

	void processNode(aiNode* node, const aiScene* scene);
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
};

#endif // !MODEL_H