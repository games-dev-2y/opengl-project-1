#include "Player.h"

Player::Player(
	const sf::Time& elapsed
	, const KeyHandler& keyHandler
	, const glm::mat4& projection
	, const glm::mat4& view
	, const GLchar* path
	, const GLchar* vertPath
	, const GLchar* fragPath
	, const glm::vec3& position

)
	: m_elapsed(elapsed)
	, m_keyHandler(keyHandler)
	, m_projectionMat(projection)
	, m_viewMat(view)
	, m_modelMat(1.0f)
	, m_normalMat(1.0f)
	, m_shader(vertPath, fragPath)
	, m_model(path)
	, m_position(position)
	, m_size(1.0f)
{
	const float size = 0.5f;
	m_normalMat = glm::scale(m_normalMat, glm::vec3(size, size, size));	// It's a bit too big for our scene, so scale it down
}

Player::~Player()
{
}

void Player::update()
{
	processInput();
	std::string msg;
	msg = "(" + std::to_string(m_position.x) + ",";
	msg += std::to_string(m_position.y) + ",";
	msg += std::to_string(m_position.z) + ")";
	DEBUG_MSG(msg.c_str());
}

void Player::render()
{
	m_modelMat = glm::translate(m_normalMat, m_position);
	m_shader.Use();

	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "projection"), 1, GL_FALSE, (&m_projectionMat[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "view"), 1, GL_FALSE, (&m_viewMat[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "model"), 1, GL_FALSE, (&m_modelMat[0][0]));

	// Draw the loaded model
	m_model.draw(m_shader);
}

const glm::vec3 & Player::getPosition() const
{
	return m_position;
}

const glm::vec3 & Player::getSize() const
{
	return m_size;
}

void Player::setPosition(const sf::Vector3f& pos)
{
	setPosition(pos.x, pos.y, pos.z);
}

void Player::setPosition(const glm::vec3& pos)
{
	setPosition(pos.x, pos.y, pos.z);
}

void Player::setPosition(const float& x, const float& y, const float& z)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;
}

void Player::move(const sf::Vector3f& offset)
{
	move(offset.x, offset.y, offset.z);
}

void Player::move(const glm::vec3& offset)
{
	move(offset.x, offset.y, offset.z);
}

void Player::move(const float& offsetX, const float& offsetY, const float& offsetZ)
{
	m_position.x += offsetX;
	m_position.y += offsetY;
	m_position.z += offsetZ;
}

void Player::processInput()
{
	const float MOVE = 15.0f * m_elapsed.asSeconds();
	if (
		m_keyHandler.isPressed(sf::Keyboard::A)
		|| m_keyHandler.isPressed(sf::Keyboard::Left)
		)
	{
		move(-MOVE, 0.0f, 0.0f);
	}
	if (
		m_keyHandler.isPressed(sf::Keyboard::D)
		|| m_keyHandler.isPressed(sf::Keyboard::Right)
		)
	{
		move(MOVE, 0.0f, 0.0f);
	}

	//// not needed
	//if (
	//	m_keyHandler.isPressed(sf::Keyboard::S)
	//	|| m_keyHandler.isPressed(sf::Keyboard::Down)
	//	)
	//{
	//	move(0.0f, -MOVE, 0.0f);
	//}
	//if (
	//	m_keyHandler.isPressed(sf::Keyboard::W)
	//	|| m_keyHandler.isPressed(sf::Keyboard::Up)
	//	)
	//{
	//	move(0.0f, MOVE, 0.0f);
	//}
}
