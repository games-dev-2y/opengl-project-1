#include "Game.h"


/// <summary>
/// Game constructor used to initialize the game and all game components
/// </summary>
Game::Game(sf::ContextSettings settings)
	: m_window(sf::VideoMode(800u, 600u, 32u), "Missile Fields", sf::Style::Default, settings)
	, m_keyHandler()
	, m_camera(m_CAM_POS, m_CAM_TARGET)
	, m_player()
	, m_gameTime(sf::Time::Zero)
	, m_splashTimer(sf::Time::Zero)
	, m_gameOver(false)
	, m_gameOverTime(sf::Time::Zero)
	, m_fadeOut(1.0f)
	, m_state(States::Loading)
	, m_text()
	, m_sprite()
{

	DEBUG_MSG(glGetString(GL_VENDOR));
	DEBUG_MSG(glGetString(GL_RENDERER));
	DEBUG_MSG(glGetString(GL_VERSION));
	DEBUG_MSG("");

	glewInit();

	auto seed = time(nullptr);

	DEBUG_MSG("Generating random seed:\n   - Seed: " + std::to_string(seed) + "\n");
	srand(static_cast<unsigned int>(seed));

	initialize();
}

/// <summary>
/// Game destructor
/// used to destroy all stack data
/// </summary>
Game::~Game()
{
}

/// <summary>
/// Game loop
/// , each update is called 60 times per second
/// , render is statically called after a full game loop
/// </summary>
void Game::run()
{
	while (m_window.isOpen())
	{
		processEvents();
		m_elapsed += m_clock.restart();
		while (m_elapsed > TIME_PER_FRAME)
		{
			m_elapsed -= TIME_PER_FRAME;
			update();
		}
		render();
	}
}

/// <summary>
/// Processes all window events
/// </summary>
void Game::processEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (m_state)
		{
		case Game::States::Splash:
		case Game::States::Game:
		case Game::States::Over:
			switch (event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			case sf::Event::KeyPressed:
				m_keyHandler.updateKey(event.key.code, true);
				break;
			case sf::Event::KeyReleased:
				m_keyHandler.updateKey(event.key.code, false);
				break;
			default:
				break;
			}
			break;
		case Game::States::Loading:
		default:
			switch (event.type)
			{
			case sf::Event::Closed:
				m_window.close();
				break;
			default:
				break;
			}
			break;
		}
	}
}

/// <summary>
/// game update logic
/// </summary>
void Game::update()
{
	switch (m_state)
	{
	case Game::States::Loading:
		setGameState(States::Splash);
		break;
	case Game::States::Splash:
		if (m_splashTimer > WAIT_TIME)
		{
			setGameState(States::Game);
		}
		break;
	case Game::States::Game:
		m_gameTime += TIME_PER_FRAME;

		m_background->update();

		m_player->update();

		m_enemies->update();

		checkCollision();
		break;
	case Game::States::Over:
		if (m_gameOverTime >= sf::microseconds(10))
		{
			m_gameOverTime *= 0.95f;
			m_gameTime += m_gameOverTime;
		}
		else
		{
			m_enemies->update();
			if (m_keyHandler.isPressed(sf::Keyboard::Space))
			{
				setGameState(States::Loading);
				m_enemies->restart();
			}
		}
		break;
	default:
		break;
	}

}

/// <summary>
/// game render logic
/// </summary>
void Game::render()
{
	switch (m_state)
	{
	case Game::States::Loading:
		break;
	case Game::States::Splash:
		m_splashTimer += m_elapsed;
		break;
	case Game::States::Game:
	case Game::States::Over:
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		m_background->render();

		m_player->render();

		m_enemies->render();

		m_window.display();
		break;
	default:
		break;
	}

}

void Game::initialize()
{
	setGameState(States::Loading);

	loadAssets();

	// Enable Depth test
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);
}

/// <summary>
/// Will contain all appropriate game collisions
/// </summary>
void Game::checkCollision()
{
	checkCollision(*m_player);
}

void Game::checkCollision(Player& p)
{
	glm::vec3 pos = p.getPosition();
	glm::vec3 size = p.getSize();

	if (pos.x < ((-m_GAME_BOUNDS.x) - size.x))
	{
		pos.x = (-m_GAME_BOUNDS.x) - size.x;
	}
	else if (pos.x > m_GAME_BOUNDS.x + size.x)
	{
		pos.x = m_GAME_BOUNDS.x + size.x;
	}
	//if (pos.y < (-m_GAME_BOUNDS.y - size.y))
	//{
	//	pos.y = -m_GAME_BOUNDS.y - size.y;
	//}
	//else if (pos.y > m_GAME_BOUNDS.y + size.y)
	//{
	//	pos.y = m_GAME_BOUNDS.y + size.y;
	//}
	p.setPosition(pos.x, pos.y, pos.z);

	if (m_enemies->checkCollisions(pos))
	{
		setGameState(States::Over);
	}
}

void Game::loadAssets()
{
	// create Player
	m_player = std::make_unique<Player>(
		TIME_PER_FRAME
		, m_keyHandler
		, m_PROJECTION
		, m_camera.m_CAMERA
		, m_PLAYER_OBJ.c_str()
		, m_MODEL_VERTEX_SHADER_FILE.c_str()
		, m_MODEL_FRAG_SHADER_FILE.c_str()
		, START_POS
		);

	// create enemy manager class
	m_enemies = std::make_unique<Enemies>(
		TIME_PER_FRAME
		, m_PROJECTION
		, m_camera.m_CAMERA
		, m_ENEMY_OBJ.c_str()
		, m_MODEL_VERTEX_SHADER_FILE.c_str()
		, m_MODEL_FRAG_SHADER_FILE.c_str()
		);

	// create background
	m_background = std::make_unique<Background>(
		m_gameTime
		, m_PROJECTION
		, m_BG_OBJ.c_str()
		, m_MODEL_VERTEX_SHADER_FILE.c_str()
		, m_BG_FRAG_SHADER_FILE.c_str()
		, m_BG_POS
		, m_BG_WIDTH
		, m_BG_HEIGHT
		);

}

void Game::setGameState(const States& state)
{

	m_state = state;

	if (m_state == States::Loading)
	{
		// Save OpenGL states
		m_window.pushGLStates();

		sf::String msg;
		msg = "Missile Field\n";
		msg += "Welcome !\nPlease wait while we load assets\n";

		if (!m_font.loadFromFile(m_FONT_FILE))
		{
			std::string s("Error loading font:\n");
			s += m_FONT_FILE;
			DEBUG_MSG(s.c_str());
			throw std::exception(s.c_str());
		}

		if (!m_texture.loadFromFile(m_SPRITE_FILE))
		{
			std::string s("Error loading texture\n");
			s += m_SPRITE_FILE;
			DEBUG_MSG(s.c_str());
			throw std::exception(s.c_str());
		}

		m_text.setFont(m_font);
		m_text.setString(msg);
		m_text.setPosition(sf::Vector2f(400.0f, 300.0f));
		auto rect = m_text.getGlobalBounds();
		m_text.setOrigin(rect.width / 2.0f, rect.height / 2.0f);
		m_text.setColor(sf::Color::Yellow);

		m_sprite.setTexture(m_texture, true);
		m_sprite.setPosition(sf::Vector2f(0.0f, 0.0f));

		m_window.clear(sf::Color::White);
		// display loading assets message
		//  as game tends to hang for
		//  a long duration when loading
		//  the high polygon count models
		m_window.draw(m_sprite);
		m_window.draw(m_text);
		m_window.display();

		// Restore OpenGL states
		m_window.popGLStates();

	}

	switch (m_state)
	{
	case Game::States::Splash:
		m_text.setString("Press Any Key to Start Game");
		break;
	case Game::States::Game:
		break;
	case Game::States::Over:
		m_gameOverTime = TIME_PER_FRAME;
		Enemies::setGameOver();
		break;
	default:
		break;
	}
}
