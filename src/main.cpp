/// <summary>
/// @mainpage OpenGL Project 1
/// @Author Rafael Girao
/// @Version 2.1
/// @brief Cube platformer using OpenGL
///
/// Total Time Taken:
///		62hr 00min
/// </summary>

#include "Game.h"

int main(void)
{
#ifndef _DEBUG
	ShowWindow(GetConsoleWindow(), SW_HIDE);
#endif // !_DEBUG

	sf::ContextSettings gameSettings;
	gameSettings.antialiasingLevel = 8u;
	gameSettings.depthBits = 32u;
	Game & game = Game(gameSettings);
	game.run();
	return EXIT_SUCCESS;
}