#include "Enemies.h"

sf::Time Enemies::s_timeTracker = sf::Time::Zero;

const float Enemies::s_START_SPEED = 15.0f;

float Enemies::s_speed = s_START_SPEED;

bool Enemies::s_gameOver = false;

/// <summary>
/// Default constructor
/// </summary>
/// <param name="elapsed">reference to constant time per update</param>
/// <param name="projection">reference to constant projection matrix</param>
/// <param name="view">reference to constant camera view matrix</param>
/// <param name="modelPath">constant c-style char model directory</param>
/// <param name="vertPath">constant c-style char vertex shader directory</param>
/// <param name="fragPath">constant c-style char frag shader directory</param>
Enemies::Enemies(
	const sf::Time& elapsed
	, const glm::mat4& projection
	, const glm::mat4& view
	, const GLchar* modelPath
	, const GLchar* vertPath
	, const GLchar* fragPath

)
	: m_ELAPSED(elapsed)
	, m_PROJECTION(projection)
	, m_VIEW(view)
	, m_model(modelPath)
	, m_shader(vertPath, fragPath)
	, m_IDENTITY(1.0f)
	, m_transform(1.0f)
	, m_spawnTrack(sf::Time::Zero)
	, m_spawnTime(sf::seconds(1.0f))
	, m_enemies()
{
}

/// <summary>
/// Default destructor,
/// removes member variables off the stack
/// </summary>
Enemies::~Enemies()
{
}

/// <summary>
/// main enemies update logic
/// </summary>
void Enemies::update()
{
	m_spawnTrack += m_ELAPSED;

	if (s_gameOver == false)
	{
		if (m_spawnTrack > m_spawnTime)
		{
			m_spawnTrack -= m_spawnTime;
			spawn();
			if (m_spawnTime > sf::seconds(1.0f))
			{
				m_spawnTime -= sf::milliseconds(200);
			}
			else if (m_spawnTime > sf::seconds(0.5f))
			{
				m_spawnTime -= sf::milliseconds(100);
			}
			else if (m_spawnTime > sf::seconds(0.2f))
			{
				m_spawnTime -= sf::microseconds(50);
			}
			else if (m_spawnTime > sf::seconds(0.1f))
			{
				m_spawnTime = sf::seconds(0.1f);
			}
		}

		s_speed += 0.2f;

		for (auto & enemy : m_enemies)
		{
			enemy->m_position.z += (s_speed * m_ELAPSED.asSeconds());
		}
	}
	else
	{
		s_speed += (1.0f * m_ELAPSED.asSeconds());
		for (auto & enemy : m_enemies)
		{
			enemy->m_position.y += s_speed * m_ELAPSED.asSeconds();
		}
	}

}

/// <summary>
/// main enemies render logic,
/// iterates through vector of enemy's struct,
/// alters transform per position and size for each enemy struct
/// </summary>
void Enemies::render()
{
	m_shader.Use();

	const auto& modelID = glGetUniformLocation(m_shader.m_program, "model");
	
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "projection"), 1, GL_FALSE, (&m_PROJECTION[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "view"), 1, GL_FALSE, (&m_VIEW[0][0]));

	for (auto & enemy : m_enemies)
	{
		m_transform = glm::translate(m_IDENTITY, enemy->m_position);
		
		glUniformMatrix4fv(modelID, 1, GL_FALSE, (&m_transform[0][0]));

		// draws our model
		m_model.draw(m_shader);
	}
}

bool Enemies::checkCollisions(const glm::vec3 & pos)
{
	const float& DE_SPAWN_POS_Z = pos.z + m_DESPAWN_DIST;
	for (int i = 0; i < m_enemies.size(); i++)
	{
		if (checkCollision(pos, *(m_enemies[i])))
		{
			setGameOver();
			return true;
		}
		if (DE_SPAWN_POS_Z < m_enemies[i]->m_position.z)
		{
			erase(i);
		}
	}
	return false;
}

/// <summary>
/// Removes enemy struct from the index,
/// using standard vector iterator 
/// </summary>
/// <param name="index"></param>
void Enemies::erase(const int& index)
{
	auto end = m_enemies.end();
	for (auto current = m_enemies.begin(); current != end; current++)
	{
		if ((*current) == m_enemies[index])
		{
			current = m_enemies.erase(current);
			end = m_enemies.end();
			break;
		}
	}
}

/// <summary>
/// static function that sets game over bool to true
/// and sets the speed to a tenth of the starting speed
/// </summary>
void Enemies::setGameOver()
{
	s_gameOver = true;
	s_speed = s_START_SPEED / 10.0f;
}

void Enemies::restart()
{
	s_gameOver = false;
	s_speed = s_START_SPEED;
	m_enemies.clear();
	m_spawnTime = sf::seconds(1.0f);
}

/// <summary>
/// spawns a enemy with a random offset
/// </summary>
void Enemies::spawn()
{
	glm::vec3 pos, size;

	size = SPAWN_SIZE;
	pos = SPAWN_POS;
	pos.z -= SPAWN_DIST;

	int randoffset = rand() % 10 - 5;
	pos.x += static_cast<float>(randoffset);

	// no need to randomize width
	//float randWidth = static_cast<float>(rand() % 100 + 50) * 0.01f;
	//size.x *= randWidth;

	m_enemies.push_back(std::make_unique<Enemy>(pos, size));
}


bool Enemies::checkCollision(const glm::vec3 & position, const Enemy & enemy)
{
	bool collided = false;
	
	const glm::vec3 & POS1 = position;
	const glm::vec3 & SIZE1 = glm::vec3(0.0f);
	const glm::vec3 & POS2 = enemy.m_position;
	const glm::vec3 & SIZE2 = m_HIT_BOX_SIZE;
	
	/* Checking for collision along all axis */
	if ( // Checking for collision along x-axis
		(POS1.x + SIZE1.x < POS2.x - SIZE2.x ||
		POS1.x > POS2.x + SIZE2.x)
		/*&& // Checking for collision along y-axis
		(POS1.y - SIZE1.y > POS2.y ||
		POS1.y < POS2.y - SIZE2.y) /* not needed */
		|| // Checking for collision along z-axis
		(POS1.z - SIZE1.z > POS2.z + (SIZE2.z * 3.0f) ||
		POS1.z < POS2.z - (SIZE2.z))
		)
	{
		collided = false;
	}
	else
	{
		collided = true;
	}
	
	return collided;
}
