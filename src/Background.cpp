#include "Background.h"


Background::Background(
	const sf::Time& time
	, const glm::mat4& projection
	, const GLchar* modelPath
	, const GLchar* vertPath
	, const GLchar* fragPath
	, const glm::vec3& position
	, const float& width
	, const float& height
)

	: m_shader(vertPath, fragPath)
	, m_model(modelPath)
	, m_position(position)
	, m_width(width)
	, m_height(height)
	, m_projectionMat(projection)
	, m_viewMat(glm::lookAt(
		glm::vec3(0.0f, 0.0f, 10.0f),	// Camera (x,y,z), in World Space
		glm::vec3(0.0f, 0.0f, 0.0f),	// Camera looking at origin
		glm::vec3(0.0f, 0.5f, 0.0f)	// 0.0f, 1.0f, 0.0f Look Down and 0.0f, -1.0f, 0.0f Look Up
	))
	, m_modelMat(glm::mat4(1.0f))
	, m_ELAPSED(time)
{
	m_modelMat = glm::translate(m_modelMat, m_position);
	m_modelMat = glm::scale(m_modelMat, glm::vec3(width, height, 1.0f));
}

/// <summary>
/// Default destructor
/// </summary>
Background::~Background()
{
}

void Background::update()
{
	return;
}

/// <summary>
/// 
/// </summary>
void Background::render()
{
	glDisable(GL_DEPTH_TEST);

	// Activate shader
	m_shader.Use();

	// send transforms to gpu
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "projection"), 1, GL_FALSE, (&m_projectionMat[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "view"), 1, GL_FALSE, (&m_viewMat[0][0]));
	glUniformMatrix4fv(glGetUniformLocation(m_shader.m_program, "model"), 1, GL_FALSE, (&m_modelMat[0][0]));

	// send time to gpu
	glUniform1f(glGetUniformLocation(m_shader.m_program, "time"), m_ELAPSED.asSeconds());

	// Draw the loaded model
	m_model.draw(m_shader);

	glEnable(GL_DEPTH_TEST);
}


//std::vector<Vertex> Background::initVertices() const
//{
//	const float screenRatio = (800.0f / 600.0f) * 1000.0f;
//
//	std::vector<Vertex> vertices;
//
//	Vertex vertice;
//	glm::vec3 vector;
//	glm::vec2 texvector;
//
//	// top-left vertex
//	vector.x = m_position.x;
//	vector.y = m_position.y - m_height;
//	vector.z = m_position.z;
//	vertice.m_position = vector;
//
//	vector.x = 0.0f;
//	vector.y = 0.0f;
//	vector.z = 0.0f;
//	vertice.m_normal = vector;
//
//	texvector.x = -screenRatio;
//	texvector.y = -screenRatio;
//	vertice.m_texCoords = texvector;
//
//	vertices.push_back(vertice);
//
//	// bottom-right
//	vector.x = m_position.x + m_width;
//	vector.y = m_position.y - m_height;
//	vector.z = m_position.z;
//	vertice.m_position = vector;
//
//	vector.x = 0.0f;
//	vector.y = 0.0f;
//	vector.z = 0.0f;
//	vertice.m_normal = vector;
//
//	texvector.x = screenRatio;
//	texvector.y = -screenRatio;
//	vertice.m_texCoords = texvector;
//
//	vertices.push_back(vertice);
//
//	// top-right
//	vector.x = m_position.x + m_width;
//	vector.y = m_position.y;
//	vector.z = m_position.z;
//
//	vector.x = 0.0f;
//	vector.y = 0.0f;
//	vector.z = 0.0f;
//	vertice.m_normal = vector;
//
//	texvector.x = screenRatio;
//	texvector.y = screenRatio;
//	vertice.m_texCoords = texvector;
//
//	vertices.push_back(vertice);
//
//	// top-left
//	vector.x = m_position.x;
//	vector.y = m_position.y;
//	vector.z = m_position.z;
//
//	vector.x = 0.0f;
//	vector.y = 0.0f;
//	vector.z = 0.0f;
//	vertice.m_normal = vector;
//
//	texvector.x = -screenRatio;
//	texvector.y = screenRatio;
//	vertice.m_texCoords = texvector;
//
//	vertices.push_back(vertice);
//	return vertices;
//}